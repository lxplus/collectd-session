<Plugin "python">
  ModulePath "/usr/libexec/sensors"
  LogTraces true
  Interactive false
  Import "puppet"
  Import "session"
  <Module "session">
  </Module>

  <Module "puppet">
    Interval "60"
  </Module>
  <Module "session">
    Domains "localhost" "cern.ch"
  </Module>
</Plugin>

